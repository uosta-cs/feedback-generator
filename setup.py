from setuptools import setup, find_packages

setup(name='stacs-feedgen',
      version='1.0.0',
      description="Feedback generator for marking CS practicals",
      long_description='This stacsfeedgen partially autoamte the feedback process by generating individual latex files for '
                       'all students being marked. It generates the pdf files and the csv file to be uploaded to MMS',
      url='https://bitbucket.org/uosta-cs/feedback-generator',
      author='Juan Jose Mendoza Santana',
      author_email='jjm20@st-andrews.ac.uk',
      classifiers=[
          'Intended Audience :: Markers :: Module coordinators',
          'Topic :: UStA-CS :: Marking',
          'Programming Language :: Python :: 3.5.2'
      ],
      packages=find_packages(),
      package_data={'stacsfeedgen': [
          'templates/feedback.cls',
          'templates/paths.txt',
          'templates/Template.tex']},
      include_package_data=True,

      )
