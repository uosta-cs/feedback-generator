import sys

from stacsfeedgen.commands.checker import CheckerCommand
from stacsfeedgen.commands.csv import CsvCommand
from stacsfeedgen.commands.init import InitCommand
from stacsfeedgen.commands.utils import Options

from stacsfeedgen.commands.pdf import PdfCommand


def execute_option(option, paths):
    if option == Options.init.name or option == Options.init.value:
        InitCommand().exec(paths)
    elif option == Options.checker.name or option == Options.checker.value:
        CheckerCommand().exec(paths)
    elif option == Options.csv.name or option == Options.csv.value:
        CsvCommand().exec(paths)
    elif option == Options.pdf.name or option == Options.pdf.value:
        PdfCommand().exec(paths)
    else:
        raise Exception('The specified option is not valid. Option chosen: {}'.format(option))


def process_tasks(options, paths):
    print('Options received: {}'.format(options))
    chosen = options.split(',')
    for option in chosen:
        execute_option(option, paths)


def build_menu():
    menu_message = 'Choose any options you want to execute followed by a comma: e.g. 1,3\n' \
                   'All options will be executed in the order specified.\n'
    for option in Options:
        menu_message += '   {} {}\n'.format(option.value, option.name)
    menu_message += '\nWhat are the tasks you want to execute? '
    return menu_message

def run(arguments):
    if len(arguments) == 3:
        task_option=arguments[1]
        paths_file=arguments[2]
    else:
        task_option = input(build_menu())
        paths_file = input('Where is your paths file (path.txt)? ')

    process_tasks(task_option, paths_file)

run(sys.argv)
