# Marker

This marker helps to automatically generate latex (.tex) files to collect the marking feedback for a specific module.

The fedback template has been taken from:
https://bitbucket.org/uosta-cs/marking_feedback_template

## Pre-requisites:
* **pdflatex** must be in your system's path

## Setup
Modify the *paths.txt* file to set up the working paths as follows:
* **assignments** The path to the directory containing all the submissions. This is the zip file downloaded from MMS unzipped
* **feedback** The path where all the feedback files (.tex) will be located.
**Be carefull** with the typos, and remember to keep the *=* sign after the variable name

You can aslo modify the *Template.tex* file located in the *templates* directory if you want all your feedback files to have any specific information.

## Usage

To generate .tex files run:
`python marker init`
This will generate a .tex file for every student whose directory was located in the path identified by the *assignments* variable set in the *paths.txt* file

To compile the .tex files and generate the .pdf files run:
`python marker pdf`
This will generate a .pdf file for every student whose .tex file  was located in path identified by the *feedback* variable set in the *paths.txt* file. This files will be found in the *pdfs* subdirectory of the *feedback* directory


## Contact

If you have any comments, feel free to use BitBucket as communication channel. Also feel free to take this development any further