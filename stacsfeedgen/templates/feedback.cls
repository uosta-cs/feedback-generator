\LoadClass{article}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{feedback}[2016/10/03 Marking feedback template]
\RequirePackage{titlesec}
\usepackage{ifthen}


\usepackage{geometry}
\geometry{
	a4paper,
	left=20mm,
	top=20mm,
}

\titleformat{\section}         % Customise the \section command 
{\Large\scshape\raggedright\bf} % Make the \section headers large (\Large),
% small capitals (\scshape) and left aligned (\raggedright)
{}{0em}                      % Can be used to give a prefix to all sections, like 'Section ...'
{}                           % Can be used to insert code before the heading


\let\@module\@empty
\let\@practicalid\@empty
\let\@practicaltitle\@empty
\let\@student\@empty

\newcommand{\module}[1]{\gdef\@module{#1}}
\newcommand{\practicalid}[1]{\gdef\@practicalid{#1}}
\newcommand{\practicaltitle}[1]{\gdef\@practicaltitle{#1}}
\newcommand{\student}[1]{\gdef\@student{#1}}
\newcommand{\sub}[1]{\large\textbf{#1}}
\newcommand{\subsub}[1]{\normalsize\textbf{#1}}

\newenvironment{alwayssingle}{
	\if@twocolumn\@restonecoltrue\onecolumn
	\else\if@openright\cleardoublepage\else\clearpage\fi
	\fi
}

\newcommand{\reqcomment}[1]
{
	\hfill
	\begin{minipage}
		{\dimexpr\textwidth-2cm}
		\textit{Comment:} #1
	\end{minipage}
	\par
}

\newcommand{\grade}[2]{
	\ifthenelse{\equal{#1}{T}}
	{\noindent{\Large\textbf{Grade:} #2}\par}
	{}
}



\newcommand{\requirement}[2]{
	\setlength{\parskip}{1mm}\noindent\hspace{10mm}$\bullet$ #1\par
	\ifthenelse{\equal{#2}{\@empty}}
	{}
	{\reqcomment{#2}}
}

\newcommand{\testsummary}[4]{
	\noindent\textbf{#1:} #2\par
	\hspace{5mm}Pass: #3\par
	\hspace{5mm}Fail: #4\par
}

\newenvironment{autochecker}
{
	\vspace*{2ex}
	\begin{multicols}{2}
}
{
	\end{multicols}
}




\newenvironment{completed}[1][Completed]{\setlength{\parskip}{1em}\noindent\subsub{#1}\par}{\par}
\newenvironment{attempted}[1][Attempted]{\setlength{\parskip}{1em}\noindent\subsub{#1}\par}{\par}
\newenvironment{missing}[1][Missing]{\setlength{\parskip}{1em}\noindent\subsub{#1}\par}{\par}


\renewcommand\maketitle{
	\begin{alwayssingle}
		\begin{center}
			{\ifx\@module\@empty\else\Large\bf\vspace*{1ex}\@module\space-\space\fi}
			\vspace*{2ex}
			{\ifx\@practical\@empty\else\Large\bf\@practicalid\space-\space\@practicaltitle\par\fi}
			{\ifx\@student\@empty\else\Large\itshape\@student\par\fi}
			\vspace*{5ex}
		\end{center}
	\end{alwayssingle}
}

\newcommand{\closefeedback}{
	\begin{center}
		\vspace*{5ex}
		\today
	\end{center}
}

\endinput
