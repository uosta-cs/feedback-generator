import os
import re
import subprocess

from stacsfeedgen.commands.command import Command

from stacsfeedgen.commands.utils import Paths, Constants


class PdfCommand(Command):
    def __process__(self):


        os.chdir(self.paths[Paths.output_dir.name])

        if not os.path.exists(Constants.PDF_PATH):
            os.makedirs(Constants.PDF_PATH)

        for feedback in os.listdir(self.paths[Paths.output_dir.name]):
            feedback_file_pattern = re.compile('[0-9]{9}.tex')
            if feedback_file_pattern.match(feedback):
                student_id = feedback.replace('.tex', '')
                print("Student id: {}".format(student_id))
                cmd = ['pdflatex', '-interaction', 'batchmode', '-halt-on-error', '-output-directory', Constants.PDF_PATH,
                       '{}.tex'.format(student_id)]
                proc = subprocess.Popen(cmd, cwd=self.paths[Paths.output_dir.name])
                proc.communicate()
                retcode = proc.returncode
                if not retcode == 0:
                    os.unlink('{}/{}.pdf'.format(Constants.PDF_PATH, student_id))
                    raise ValueError('Error {} executing command: {}'.format(retcode, ' '.join(cmd)))

                os.unlink('{}/{}.log'.format(Constants.PDF_PATH, student_id))
                os.unlink('{}/{}.aux'.format(Constants.PDF_PATH, student_id))
