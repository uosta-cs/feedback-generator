import os
import re

from stacsfeedgen.commands.command import Command

from stacsfeedgen.commands.utils import Paths


class AutocheckerSummary():
    def __init__(self):
        self.public_basic_pass = 0
        self.public_basic_fail = 0
        self.public_enhancement_pass = 0
        self.public_enhancement_fail = 0
        self.private_basic_pass = 0
        self.private_basic_fail = 0
        self.private_enhancement_pass = 0
        self.private_enhancement_fail = 0
        self.other_pass = 0
        self.other_fail = 0

    def total_public_basic(self):
        return self.public_basic_pass + self.public_basic_fail

    def total_private_basic(self):
        return self.private_basic_pass + self.private_basic_fail

    def total_basic_pass(self):
        return self.private_basic_pass + self.public_basic_pass

    def total_basic_fail(self):
        return self.private_basic_fail + self.public_basic_fail

    def total_basic(self):
        return self.total_basic_pass() + self.total_basic_fail()

    def total_public_enhancement(self):
        return self.public_enhancement_pass + self.public_enhancement_fail

    def total_private_enhancement(self):
        return self.private_enhancement_pass + self.private_enhancement_fail

    def total_enhancement_pass(self):
        return self.private_enhancement_pass + self.public_enhancement_pass

    def total_enhancement_fail(self):
        return self.private_enhancement_fail + self.public_enhancement_fail

    def total_enhancement(self):
        return self.total_enhancement_pass() + self.total_enhancement_fail()

    def total_other(self):
        return self.other_pass + self.other_fail

    def total_pass(self):
        return self.total_basic_pass() + self.total_enhancement_pass() + self.other_pass

    def total_fail(self):
        return self.total_basic_fail() + self.total_enhancement_fail() + self.other_fail

    def total(self):
        return self.total_pass() + self.total_fail()

    def update(self, line):
        private_basic_pass = re.compile('(.*) - private/basic(.*): pass\n')
        private_basic_fail = re.compile('(.*) - private/basic(.*): fail\n')
        public_basic_pass = re.compile('(.*) - basic(.*): pass\n')
        public_basic_fail = re.compile('(.*) - basic(.*): fail\n')
        private_enhancement_pass = re.compile('(.*) - private/enhancement(.*): pass\n')
        private_enhancement_fail = re.compile('(.*) - private/enhancement(.*): fail\n')
        public_enhancement_pass = re.compile('(.*) - enhancement(.*): pass\n')
        public_enhancement_fail = re.compile('(.*) - enhancement(.*): fail\n')
        other_pass = re.compile('(.*): pass\n')
        other_fail = re.compile('(.*): fail\n')

        if private_basic_pass.match(line):
            self.private_basic_pass += 1
        elif private_basic_fail.match(line):
            self.private_basic_fail += 1
        elif public_basic_pass.match(line):
            self.public_basic_pass += 1
        elif public_basic_fail.match(line):
            self.public_basic_fail += 1
        elif private_enhancement_pass.match(line):
            self.private_enhancement_pass += 1
        elif private_enhancement_fail.match(line):
            self.private_enhancement_fail += 1
        elif public_enhancement_pass.match(line):
            self.public_enhancement_pass += 1
        elif public_enhancement_fail.match(line):
            self.public_enhancement_fail += 1
        elif other_pass.match(line):
            self.other_pass += 1
        elif other_fail.match(line):
            self.other_fail += 1

    def to_string(self):
        summary = ''
        summary += r'\n\\testsummary{Total tests}{' + str(self.total()) + '}{' + str(self.total_pass()) + '}{' + str(
            self.total_fail()) + '}'
        summary += r'\n\\testsummary{Total basic tests}{' + str(self.total_basic()) + '}{' + str(
            self.total_basic_pass()) + '}{' + str(self.total_basic_fail()) + '}'
        summary += r'\n\\testsummary{Total public basic tests}{' + str(self.total_public_basic()) + '}{' + str(
            self.public_basic_pass) + '}{' + str(self.public_basic_fail) + '}'
        summary += r'\n\\testsummary{Total private basic tests}{' + str(self.total_private_basic()) + '}{' + str(
            self.private_basic_pass) + '}{' + str(self.private_basic_fail) + '}'
        summary += r'\n\\testsummary{Total enhancement tests}{' + str(self.total_enhancement()) + '}{' + str(
            self.total_enhancement_pass()) + '}{' + str(self.total_enhancement_fail()) + '}'
        summary += r'\n\\testsummary{Total public enhancement tests}{' + str(
            self.total_public_enhancement()) + '}{' + str(
            self.public_enhancement_pass) + '}{' + str(self.public_enhancement_fail) + '}'
        summary += r'\n\\testsummary{Total private enhancement tests}{' + str(
            self.total_private_enhancement()) + '}{' + str(self.private_enhancement_pass) + '}{' + str(
            self.private_enhancement_fail) + '}'
        summary += r'\n\\testsummary{Total other tests}{' + str(self.total_other()) + '}{' + str(
            self.other_pass) + '}{' + str(self.other_fail) + '}\n'

        return summary


class CheckerCommand(Command):
    def __process__(self):
        for student_feedback in os.listdir(self.paths[Paths.output_dir.name]):
            feedback_file_pattern = re.compile('[0-9]*\.tex')
            if feedback_file_pattern.match(student_feedback):
                student_id = student_feedback.split('.')[0]
                print('processing autochecker for student : {}'.format(student_id))
                autochecker_file = '{}\{}.txt'.format(self.paths[Paths.autochecker_dir.name], student_id)
                if os.path.exists(autochecker_file):
                    summary = AutocheckerSummary()
                    f = open(autochecker_file, 'r')
                    for line in f:
                        test_pattern = re.compile('\* (.*)TEST (.*)\n')
                        if test_pattern.match(line):
                            summary.update(line)
                    f.close()
                    autocheker_summary = summary.to_string()
                else:
                    autocheker_summary = '\nNo autochecker information available\n'
                feedback_file = '{}\{}.tex'.format(self.paths[Paths.output_dir.name], student_id)
                f = open(feedback_file, 'r')
                feedback_content = f.read()
                f.close()

                autochekcer_regex = r'\\begin{autochecker}(.*\n)*\\end{autochecker}'
                feedback_content = re.sub(autochekcer_regex, r'\\begin{autochecker}' + autocheker_summary + '\\end{autochecker}',
                                          feedback_content)

                f = open(feedback_file, 'w')
                f.write(feedback_content)
                f.close()

