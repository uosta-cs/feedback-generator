import os
from enum import Enum


class Options(Enum):
    init = '1'
    checker = '2'
    csv = '3'
    pdf = '4'


class Paths(Enum):
    submissions_dir = 1
    autochecker_dir = 2
    template_file = 3
    output_dir = 4


class Constants:
    HERE = os.path.dirname(os.path.abspath(__file__))
    LATEX_CLASS_NAME = 'feedback.cls'
    TEMPLATE_FILE = os.path.join(HERE, '../templates/Template.tex')
    CLASS_FILE = os.path.join(HERE, '../templates/{}'.format(LATEX_CLASS_NAME))
    PDF_PATH = 'pdf/'
    DEFAULT_FEEDBACK = 'See attached feedback'


def check_path(path):
    if not os.path.exists(path):
        raise Exception('ERROR - {} not found'.format(path))


class PathsReader:
    def __init__(self, paths_file):
        check_path(paths_file)
        self.paths_file = paths_file
        self.paths = {}

    def get_paths(self):
        f = open(self.paths_file, 'r')
        for line in f:
            line_contents = line.split('=')
            path_name = line_contents[0].strip()
            path_value = line_contents[1].strip().replace('\n', '')
            check_path(path_value)
            self.paths[path_name] = os.path.abspath(path_value)
        f.close()

        return self.paths
