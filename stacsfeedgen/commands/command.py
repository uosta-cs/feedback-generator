from abc import ABCMeta, abstractmethod

from stacsfeedgen.commands.utils import PathsReader


class Command(object, metaclass=ABCMeta):

    def __init__(self):
        self.paths = {}

    @abstractmethod
    def __process__(self):
        pass

    def exec(self, paths_file):
        self.paths = PathsReader(paths_file).get_paths()
        print("===========================================\n"
              "Initiating command: {}\n"
              "===========================================\n".format(self.__class__.__name__))
        self.__process__()
