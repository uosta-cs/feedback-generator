import os
import re

from stacsfeedgen.commands.command import Command

from stacsfeedgen.commands.utils import Constants
from stacsfeedgen.commands.utils import Paths


class InitCommand(Command):
    def __process__(self):

        f = open(self.paths[Paths.template_file.name], 'r', encoding="utf8")
        latex_template = f.read()
        f.close()

        for submission in os.listdir(self.paths[Paths.submissions_dir.name]):
            if submission.isdigit():
                print('Generating feedback templates for student {}'.format(submission))
                file_content = re.sub('student\\{[0-9]+\\}', 'student{' + submission + '}', latex_template)
                f = open('{}\\{}.tex'.format(self.paths[Paths.output_dir.name], submission), 'w', encoding="utf8")
                f.write(file_content)
                f.close()

        f = open(Constants.CLASS_FILE, 'r')
        class_template = f.read()
        f.close()

        f = open('{}\{}'.format(self.paths[Paths.output_dir.name], Constants.LATEX_CLASS_NAME), 'w')
        f.write(class_template)
        f.close()
