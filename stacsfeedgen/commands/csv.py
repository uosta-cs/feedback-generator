import os
import re

from stacsfeedgen.commands.command import Command

from stacsfeedgen.commands.utils import Paths, Constants


class CsvCommand(Command):
    def __process__(self):





        grades_file_content = ''

        for feedback in os.listdir(self.paths[Paths.output_dir.name]):
            feedback_file_pattern = re.compile('[0-9]{9}.tex')
            if feedback_file_pattern.match(feedback):
                student_id = feedback.replace('.tex', '')
                f = open('{}\{}'.format(self.paths[Paths.output_dir.name], feedback), 'r', encoding="utf8")
                feedback_content = f.read()
                f.close()
                grade_segments = feedback_content.split(r'\grade{')
                if len(grade_segments)>=2:
                    grade = grade_segments[1]
                    grade = grade.split(r'}')[0]
                    student_entry = '{},{},,{}\n'.format(student_id, grade, Constants.DEFAULT_FEEDBACK)
                else:
                    student_entry = '{},{},,{}\n'.format(student_id, 'NS', Constants.DEFAULT_FEEDBACK)

                print(student_entry)
                grades_file_content += student_entry

        f = open('{}\grades.csv'.format(self.paths[Paths.output_dir.name]), 'w')
        f.write(grades_file_content)
        f.close()