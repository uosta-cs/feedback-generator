# Feedback Generator

This program automatically generates latex feedback templates for every student. 

The template can be customized for each practical and module. 

For instance, the module coordinator should fill the basic template and then distribute it to the marking team.

If autochecker feedback is available, it will be summarize for every student in the template.

You can see the [feedback preview](stacsfeedgen/templates/Template.pdf) in:

>[stacsfeedgen/templates/Template.pdf](stacsfeedgen/templates/Template.pdf) 

The basic [template](stacsfeedgen/templates/Template.tex) can be found in:
 
>[stacsfeedgen/templates/Template.tex](stacsfeedgen/templates/Template.tex)

The template uses the latex class [feedback.cls ](stacsfeedgen/templates/feedback.cls) to decorate the containing elements. It can be found in 

>[stacsfeedgen/templates/feedback.cls](stacsfeedgen/templates/feedback.cls)

## Pre-requisites:
* **pdflatex** must be in your system's path
* **python 3** musts be installed, it is not neccesary to have in in the classpath
* **pip**(optional) can be use to install the package

## Usage

1. Install the generator as a python package using pip.

    `pip install --upgrade git+https://bitbucket.org/uosta-cs/feedback-generator.git@master#egg=stacs-feedgen`

2. Setup the following paths in a [path.txt](stacsfeedgen/templates/paths.txt) file like the ones set in

    > submissions_dir = path\to\your\submissions\directory
    >
    > autochecker_dir = path\to\your\autochecker\output\directory
    >
    > template_file = path\to\your\customized\Template.tex
    >
    > output_dir = path\to\place\the\feedback\files

    A sample file can be found in:
    [stacsfeedgen/templates/paths.txt](stacsfeedgen/templates/paths.txt) 

3. Open a python shell

    `python`

4. Run the generator typing the folliwig command in the python shell:

    `>>import stacsfeedgen.feedgen`

5. At this point the generator should be running and  you should see something like this:

    ![Generator menu](doc/img/generator_menu.png)

6. Follow the steps choosing any of the following options:

    > **1 init** 
    >
    > > Generates one marking file for every student found in _submissions_dir_. 
    >
    > > The files can be found in  _output_dir_.
    >
    > > It copies all the information contained in _template_file_
    >
    > **2 checker**
    >
    > > Process the autochecker output from _autochecker_dir_ and fills every student's template with a summary.
    >
    > **3 csv**
    >
    > > Generates a csv file with all the grades.
    >
    > > The file is located in _output_dir_ as grades.csv
    >
    > **4 pdf**
    >
    >> Generates all the pdf files from the templates
    >
    >> The files are located in the directory _pdf_ inside _output_dir_
    
    
7. If you want, you can also download the source code.

    `git clone https://bitbucket.org/uosta-cs/feedback-generator.git`

8. Then you can run it as

    `python feedgen.py [options][paths]`
    
    >**options** - The optiosn to execute. e.g. 1,3
    >
    >**paths** - The path to the file paths.txt



## Contact

If you have any comments, feel free to use BitBucket as communication channel. 
You are also very welcome to take this development any further